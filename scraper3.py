import requests
import re
from bs4 import BeautifulSoup
import csv
import time
import gc
import sqlite3

ITEMS_PPAGE = 3 # for debugging, limit max requests per page
PAGES_ITER = 3 # for debugging, limited numof subpages

def vendorpage_retrieve(href):

    rv = requests.get(href)
    REQ_SUCC_CODE = 200
    success = (rv.status_code == REQ_SUCC_CODE)

    '''
    ISSUE n.1:
    Python high memory usage with BeautifulSoup
    https://stackoverflow.com/questions/11284643/python-high-memory-usage-with-beautifulsoup
    '''
    page = BeautifulSoup(rv.content, 'html.parser')


    try:
        item_id = str(page.find_all("h1", {"class": "wt-text-body-03 wt-line-height-tight wt-break-word wt-mb-xs-1"}))
        item_id = int(item_id.split('"')[5])
    except:
        item_id = None

    try:
        work_name = str(page.find("h1", {"class": "wt-text-body-03 wt-line-height-tight wt-break-word wt-mb-xs-1"}))
        work_name = work_name.splitlines()[1].lstrip()
    except:
        work_name = None

    try:
        price = float(str(page.find("h3", {"class": "wt-text-title-03 wt-mr-xs-2"})).split()[3][1:-1])
    except:
        price = None

    try:
        old_price = str(page.find("p", {"class": "wt-text-strikethrough wt-text-caption wt-text-gray wt-mr-xs-1"}))
        old_price = float(re.sub('[^0-9.]', "", ((old_price.split("<")[1]).split(">")[1][1:-1])))
    except:
        old_price = None

    try:
        elements = str(page.find_all("a", {"class": "wt-text-link-no-underline"}))
        target = elements.find("store owner")
        store_owner = (elements[target + 12:]).split('"')[0]
    except:
        store_owner = None

    try:
        gallery_rank = str(page.find_all("span", {"class": "wt-text-body-01"})[1])
        gallery_rank = (gallery_rank.splitlines()[1]).split()[0]
    except:
        gallery_rank = None

    try:
        store_seniority = str(page.find_all("p", {"class": "wt-text-body-03 wt-line-height-tight"})[2])
        store_seniority = (store_seniority.split(">")[1]).split("<")[0]
    except:
        store_seniority = None
    try:
        store_country = str(page.find_all("p", {"class": "wt-text-body-03 wt-line-height-tight"})[3])
        store_country = (store_country.split(">")[1]).split("<")[0]
    except:
        store_country = None

    # try:
    #     performance_method = \
    #     (str(page.find_all("p", {"class": "wt-text-caption wt-text-gray wt-ml-mb-2"}))).splitlines()[1].split()[0]
    performance_method = 0


    res =    [item_id, work_name, price, old_price, store_owner, store_seniority, gallery_rank, store_country, performance_method]


    page.decompose()    # Georges: from ISSUE n.1
    gc.collect()
    rv.close()

    return res


def iterate_vendorpages(etsy_cat_root, pagenum):

    artworks_data = []      # initialize after each page

    print('start catsubpage', time.process_time())
    etsy_cat_pagenum = etsy_cat_root + str(pagenum)
    rq = requests.get(etsy_cat_pagenum)
    REQ_SUCC_CODE = 200
    success = (rq.status_code == REQ_SUCC_CODE)
    print('requested catsubpage', time.process_time())
    soup = BeautifulSoup(rq.content, 'html.parser')
    print('loaded catsubpage', time.process_time(), '\n')

    tags = soup.find_all('a', class_='display-inline-block listing-link')
    if len(tags) <= 0:
        tags = soup.find_all('a', class_=' display-inline-block listing-link ')

    i =  1 #limit counter
    if len(tags)>0:
        for t in tags:
            with requests.get(t.attrs['href']) as r:
                if r.status_code == REQ_SUCC_CODE:
                    ti = time.process_time()
                    artworks_data.append( vendorpage_retrieve(t.attrs['href']) )
                    print('retrieved page n.' + str(pagenum) + ', item n.' + str(i)+', t:' + str(time.process_time() - ti))
                else:
                    continue
            i += 1
            if i > ITEMS_PPAGE: break
    else:
        success = False

    soup.decompose()    # Georges: from ISSUE n.1
    gc.collect()
    rq.close()
    print ('---> finished page n.'+str(pagenum)+' result: '+str(success))

    with open('art_works.csv', 'a') as file:     # Georges temporarily write to csv to check out. later DataBase
        csv_file = csv.writer(file)
        j = 0
        for item in artworks_data:
            fields = item
            csv_file.writerow(fields)
            j+=1

    print('---> write to csv ' + str(j) + ' lines \n')

    con = sqlite3.connect(TEST_DB_FILENAME)
    cur = con.cursor()
    for item in artworks_data:
        # if (type(item) is list):
        fieldstring = "','".join(map(str, item))
        sqlstatmt = "INSERT INTO interface_etsy VALUES ('"+fieldstring+"');"
        print(sqlstatmt)
        cur.execute(sqlstatmt)

    con.commit()
    con.close()


    return success & (len(tags) > 0)

def iterate_category(etsy_cat_root):
    success = True
    i = 1
    while (success & (i < PAGES_ITER)):
        success = iterate_vendorpages(etsy_cat_root, i)
        i += 1
    return i


if __name__ == '__main__':
    etsy_cat_pagenum = 'https://www.etsy.com/il-en/c/art-and-collectibles/painting/acrylic?ref=pagination&explicit=1&page=2'
    etsy_cat_root = 'https://www.etsy.com/il-en/c/art-and-collectibles/painting/acrylic?ref=pagination&explicit=1&page='

    TEST_DB_FILENAME = 'etsy_scrape.db'

    con = sqlite3.connect(TEST_DB_FILENAME)

    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS interface_etsy
                (ID INTEGER, 
                title TEXT,
                price FLOAT,
                price_old FLOAT,
                author TEXT,
                YEAR INTEGER,
                dummy TEXT,
                origin TEXT,
                rank INTEGER)
                '''
                )

    artworks_data = []      # put it to root, tmeporarily

    print('start ',time.process_time())
    #success = iterate_vendorpages(etsy_cat_pagenum)
    #print('success: ', success)

    result = iterate_category(etsy_cat_root)
    print('retr pages: ', result)

