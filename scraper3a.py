import requests
import re
from bs4 import BeautifulSoup
import csv
import time
import gc
import sqlite3

ITEMS_PPAGE = 3  # for debugging, limit max requests per page
PAGES_ITER = 3  # for debugging, limited numof subpages


def vendorpage_retrieve(href):
    rv = requests.get(href)
    REQ_SUCC_CODE = 200
    success = (rv.status_code == REQ_SUCC_CODE)

    '''
    ISSUE n.1:
    Python high memory usage with BeautifulSoup
    https://stackoverflow.com/questions/11284643/python-high-memory-usage-with-beautifulsoup
    '''
    page = BeautifulSoup(rv.content, 'html.parser')

    def get_text(txt_str):
        """
        recives an bs4 tag and returns the text as string
        """
        return " ".join(txt_str.get_text().strip('\n').strip('\t').split())

    def get_num(num_str):
        """
        recives an bs4 tag and returns the float value
        """
        return float(re.sub('[^0-9.]', '', num_str.get_text().strip('\n').strip('\t')))

    try:
        item_id = page.find("h1", {"class": "wt-text-body-03 wt-line-height-tight wt-break-word wt-mb-xs-1"})
        item_id = int(id.attrs['data-listing-id'])
    except AttributeError:
        item_id = None

        work_name = page.find("h1", {"class": "wt-text-body-03 wt-line-height-tight wt-break-word wt-mb-xs-1"})
        work_name = get_text(work_name)
    except AttributeError:
        work_name = None

        price = page.find("h3", {"class": "wt-text-title-03 wt-mr-xs-2"})
        price = get_num(price)
    except AttributeError:
        price = None

        old_price = page.find("p", {"class": "wt-text-strikethrough wt-text-caption wt-text-gray wt-mr-xs-1"})
        old_price = get_num(old_price)
    except AttributeError:
        old_price = None

        elements = str(page.find_all("a", {"class": "wt-text-link-no-underline"}))
        name_index_init = elements.index("store owner") + len("store owner")
        name_index_end = elements[name_index_init:].index('"') + name_index_init
        store_name = (elements[name_index_init:name_index_end])
    except AttributeError:
        store_name = None

        store_rank = page.find("a", {
            "class": "wt-text-link-no-underline wt-display-inline-flex-xs wt-align-items-center wt-nudge-b-2"})
        store_rank = float(store_rank.find("input").attrs['value'])
    except AttributeError:
        store_rank = None

        store_start_year = page.find("p", {"class": "wt-text-caption wt-text-gray wt-mb-xs-1"}, text="On Etsy since")
        store_start_year = int(get_text(store_start_year.next_sibling.next_sibling))
    except AttributeError:
        store_start_year = None

        store_country = page.find("p", {"class": "wt-text-caption wt-text-gray wt-mb-xs-1"}, text="Based in")
        store_country = get_text(store_country.next_sibling.next_sibling)
    except AttributeError:
        store_country = None

        store_number_of_sales = page.find("p", {"class": "wt-text-caption wt-text-gray wt-mb-xs-1"}, text="Sales")
        store_number_of_sales(get_text(store_number_of_sales.next_sibling.next_sibling))
    except AttributeError:
        store_number_of_sales = None

        number_of_ranks = page.find("p", {"class": "wt-text-caption wt-text-gray wt-mb-xs-1"},
                                    text="Total shop reviews")
        print(get_text(number_of_ranks.next_sibling.next_sibling))
    except AttributeError:
        number_of_ranks = None

        performance_method = (page.find("p", {"class": "wt-text-caption wt-text-gray wt-ml-mb-2"}))
        performance_method = get_text(performance_method)
    except AttributeError:
        performance_method = None



    res = [item_id, work_name, price, old_price, store_name, store_number_of_sales, store_start_year, store_rank, number_of_ranks, performance_method, store_country]

    page.decompose()  # Georges: from ISSUE n.1
    gc.collect()
    rv.close()

    return res


def iterate_vendorpages(etsy_cat_root, pagenum):
    artworks_data = []  # initialize after each page

    print('start catsubpage', time.process_time())
    etsy_cat_pagenum = etsy_cat_root + str(pagenum)
    rq = requests.get(etsy_cat_pagenum)
    REQ_SUCC_CODE = 200
    success = (rq.status_code == REQ_SUCC_CODE)
    print('requested catsubpage', time.process_time())
    soup = BeautifulSoup(rq.content, 'html.parser')
    print('loaded catsubpage', time.process_time(), '\n')

    tags = soup.find_all('a', class_='display-inline-block listing-link')
    if len(tags) <= 0:
        tags = soup.find_all('a', class_=' display-inline-block listing-link ')

    i = 3  # limit counter
    if len(tags) > 0:
        for t in tags:
            with requests.get(t.attrs['href']) as r:
                if r.status_code == REQ_SUCC_CODE:
                    ti = time.process_time()
                    artworks_data.append(vendorpage_retrieve(t.attrs['href']))
                    print('retrieved page n.' + str(pagenum) + ', item n.' + str(i) + ', t:' + str(
                        time.process_time() - ti))
                else:
                    continue
            i += 1
            if i > ITEMS_PPAGE: break
    else:
        success = False

    soup.decompose()  # Georges: from ISSUE n.1
    gc.collect()
    rq.close()
    print('---> finished page n.' + str(pagenum) + ' result: ' + str(success))

    with open('art_works.csv', 'a') as file:  # Georges temporarily write to csv to check out. later DataBase
        csv_file = csv.writer(file)
        j = 0
        for item in artworks_data:
            fields = item
            csv_file.writerow(fields)
            j += 1

    print('---> write to csv ' + str(j) + ' lines \n')

    con = sqlite3.connect(TEST_DB_FILENAME)
    cur = con.cursor()
    for item in artworks_data:
        # if (type(item) is list):
        fieldstring = "','".join(map(str, item))
        sqlstatmt = "INSERT INTO interface_etsy VALUES ('" + fieldstring + "');"
        print(sqlstatmt)
        cur.execute(sqlstatmt)

    con.commit()
    con.close()

    return success & (len(tags) > 0)


def iterate_category(etsy_cat_root):
    success = True
    i = 1
    while (success & (i < PAGES_ITER)):
        success = iterate_vendorpages(etsy_cat_root, i)
        i += 1
    return i


if __name__ == '__main__':
    etsy_cat_pagenum = 'https://www.etsy.com/il-en/c/art-and-collectibles/painting/acrylic?ref=pagination&explicit=1&page=2'
    etsy_cat_root = 'https://www.etsy.com/il-en/c/art-and-collectibles/painting/acrylic?ref=pagination&explicit=1&page='

    TEST_DB_FILENAME = 'etsy_scrape.db'

    con = sqlite3.connect(TEST_DB_FILENAME)

    cur = con.cursor()
    cur.execute('''CREATE TABLE IF NOT EXISTS interface_etsy
                (ID INTEGER, 
                title TEXT,
                price FLOAT,
                price_old FLOAT,
                store TEXT,
                store_sales INT,
                store_YEAR INTEGER,
                store_rank FLOAT,
                num_of_ranker INT,
                method TEXT,
                origin TEXT)
                '''
                )

    artworks_data = []  # put it to root, tmeporarily

    print('start ', time.process_time())
    # success = iterate_vendorpages(etsy_cat_pagenum)
    # print('success: ', success)

    result = iterate_category(etsy_cat_root)
    print('retr pages: ', result)
