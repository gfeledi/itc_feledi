import requests
import re
from bs4 import BeautifulSoup
import csv

def vendorpage_retrieve(href):
    r = requests.get(href)
    REQ_SUCC_CODE = 200
    success = (r.status_code == REQ_SUCC_CODE)

    page = BeautifulSoup(r.content, 'html.parser')
    try:
        item_id = str(page.find_all("h1", {"class": "wt-text-body-03 wt-line-height-tight wt-break-word wt-mb-xs-1"}))
        item_id = int(item_id.split('"')[5])
    except:
        item_id = None

    try:
        work_name = str(page.find("h1", {"class": "wt-text-body-03 wt-line-height-tight wt-break-word wt-mb-xs-1"}))
        work_name = work_name.splitlines()[1].lstrip()
    except:
        work_name = None

    try:
        price = float(str(page.find("h3", {"class": "wt-text-title-03 wt-mr-xs-2"})).split()[3][1:-1])
    except:
        price = None

    try:
        old_price = str(page.find("p", {"class": "wt-text-strikethrough wt-text-caption wt-text-gray wt-mr-xs-1"}))
        old_price = float(re.sub('[^0-9.]', "", ((old_price.split("<")[1]).split(">")[1][1:-1])))
    except:
        old_price = None

    try:
        elements = str(page.find_all("a", {"class": "wt-text-link-no-underline"}))
        target = elements.find("store owner")
        store_owner = (elements[target + 12:]).split('"')[0]
    except:
        store_owner = None

    try:
        gallery_rank = str(page.find_all("span", {"class": "wt-text-body-01"})[1])
        gallery_rank = (gallery_rank.splitlines()[1]).split()[0]
    except:
        gallery_rank = None

    try:
        store_seniority = str(page.find_all("p", {"class": "wt-text-body-03 wt-line-height-tight"})[2])
        store_seniority = (store_seniority.split(">")[1]).split("<")[0]
    except:
        store_seniority = None
    try:
        store_country = str(page.find_all("p", {"class": "wt-text-body-03 wt-line-height-tight"})[3])
        store_country = (store_country.split(">")[1]).split("<")[0]
    except:
        store_country = None

    # try:
    #     performance_method = \
    #     (str(page.find_all("p", {"class": "wt-text-caption wt-text-gray wt-ml-mb-2"}))).splitlines()[1].split()[0]
    performance_method = 0

    artworks_data.append(
        [item_id, work_name, price, old_price, store_owner, store_seniority, gallery_rank,
         store_country, performance_method])
    return success


def iterate_vendorpages(etsy_cat_pagenum):
    r = requests.get(etsy_cat_pagenum)
    REQ_SUCC_CODE = 200
    success = (r.status_code == REQ_SUCC_CODE)

    soup = BeautifulSoup(r.content, 'html.parser')

    tags = soup.find_all('a', class_='display-inline-block listing-link')
    if len(tags) <= 0:
        tags = soup.find_all('a', class_=' display-inline-block listing-link ')

    i =  1 #limit counter
    for t in tags:
        with requests.get(t.attrs['href']) as r:
            if r.status_code == REQ_SUCC_CODE:
                print('retriving page n.'+str(i))
                statuscode = vendorpage_retrieve(t.attrs['href'])
            else:
                continue
        i += 1
        if i > 10: break

    return success & (len(tags) > 0)


if __name__ == '__main__':
    etsy_cat_pagenum = 'https://www.etsy.com/il-en/c/art-and-collectibles/painting/acrylic?ref=pagination&explicit=1&page=2'
    artworks_data = []

    success = iterate_vendorpages(etsy_cat_pagenum)
    print('success: ', success)
    #print(artworks_data)

    with open('artworks.csv', 'w') as file:
        csv_file = csv.writer(file)
        for item in artworks_data:
            fields = item
            csv_file.writerow(fields)